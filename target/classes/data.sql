INSERT INTO PRODUCT (name, description, parent_id) VALUES
('Product 1', 'Desc of Product 1', null),
('Product 2', 'Desc of Product 2', 1),
('Product 3', 'Desc of Product 3', 1);

INSERT INTO IMAGE(type, product_id) VALUES
('JPG', 1),
('PNG', 2),
('GIF', 3);
