How to run the code:

1. After clone the repository, open in the Eclipe program, for example:
	1.1. Click em Run As -> Maven build;
	1.2. On Goals line, write tomcat7:run and click on run
	1.3. Check if it's running going to http://localhost:8080/
	1.4. See the CRUD Products Davi Menezes! message
2. Open a Postman program or similar
	2.1. Use GET operation with the URL http://localhost:8080/apirest/product/list/ for listing the products
	2.2. Use GET operation with the URL http://localhost:8080/apirest/image/list/ for listing the images
	2.3. Use GET operation with the URL http://localhost:8080/apirest/product/list/{product_id} for listing a specific product
	2.4. Use GET operation with the URL http://localhost:8080/apirest/image/list/{product_id} for listing a specific product
	2.5. Use GET operation with the URL http://localhost:8080/apirest/list/children/{parent_id} for listing all children for a specific parent
	2.6. Use GET operation with the URL http://localhost:8080/apirest/list/product/{product_id} for listing all images for a specific product
	2.7. Use POST operation with the URL http://localhost:8080/apirest/product/insert for inserting a product:
		2.7.1. Use a similar JSON: { "name": "Product 1", "description": "Desc" }
	2.8. Use POST operation with the URL http://localhost:8080/apirest/image/insert for inserting an image:
		2.8.1. Use a similar JSON: { "type": "JPEG", "product_id": "1" }
	2.9. Use PUT operation with the URL http://localhost:8080/apirest/product/update for updating a product:
		2.9.1. Use a similar JSON: { "name": "Product 1", "description": "Desc" }
	2.10. Use PUT operation with the URL http://localhost:8080/apirest/image/update for updating a product:
		2.10.1. Use a similar JSON: { "type": "JPEG", "product_id": "1" }
	2.11. Use DELETE operation with the URL http://localhost:8080/apirest/remove/{product_id} for delete a product
	2.12. Use DELETE operation with the URL http://localhost:8080/apirest/remove/{image_id} for delete an image
3. Have fun
