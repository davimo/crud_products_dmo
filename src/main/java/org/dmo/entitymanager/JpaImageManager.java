package org.dmo.entitymanager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaImageManager {
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("app_image");
	private EntityManager em = factory.createEntityManager();
	
	public EntityManager getEntityManager(){
		return em;
	}
}
