package org.dmo.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.dmo.entitymanager.JpaImageManager;
import org.dmo.model.ImageModel;

@Path("/image")
public class ImageService {
	private JpaImageManager JPAEM = new JpaImageManager();
	private EntityManager objEM = JPAEM.getEntityManager();
	
	@GET
	@Path("/list")
	@Produces("application/json")
	public List<ImageModel> list() {
		try {
			String query = "select img from ImageModel img";
			List<ImageModel> images = objEM.createQuery(query, ImageModel.class).getResultList();
			objEM.close();
			
			return images;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@GET
	@Path("/list/{image_id}")
	@Produces("application/json")
	public ImageModel getById(@PathParam("image_id") Long image_id){
		try {
			ImageModel image = objEM.find(ImageModel.class, image_id);
			objEM.close();
			
			return image;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@GET
	@Path("/list/product/{product_id}")
	@Produces("application/json")
	public List<ImageModel> getByProduct(@PathParam("product_id") Long product_id) {
		try {
			String query = "select img from ImageModel img where product_id = " + product_id;
			List<ImageModel> images = objEM.createQuery(query, ImageModel.class).getResultList();
			objEM.close();
			
			return images;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/insert")
	@Consumes("application/json")
	public Response insert(ImageModel objImage){	
		ImageModel image = new ImageModel();
		image.setType(objImage.getType());
		image.setProduct(objImage.getProduct());
		
		try {
			objEM.persist(image);
			objEM.close();
			return Response.status(200).entity("image inserted.").build();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(ImageModel objImage){
		try {
			objEM.getTransaction().begin();
			objEM.merge(objImage);
			objEM.getTransaction().commit();
			objEM.close();
			return Response.status(200).entity("image updated.").build();
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@DELETE
	@Path("/remove/{image_id}")
	public Response delete(@PathParam("image_id") Long image_id){
		try {
			ImageModel objImage = objEM.find(ImageModel.class, image_id);
			objEM.getTransaction().begin();
			objEM.remove(objImage);
			objEM.getTransaction().commit();
			objEM.close();
			return Response.status(200).entity("image deleted.").build();
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
}
