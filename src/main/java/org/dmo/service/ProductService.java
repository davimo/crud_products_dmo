package org.dmo.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.dmo.entitymanager.JpaEntityManager;
import org.dmo.model.ProductModel;

@Path("/product")
public class ProductService {
	private JpaEntityManager JPAEM = new JpaEntityManager();
	private EntityManager objEM = JPAEM.getEntityManager();
	
	@GET
	@Path("/list")
	@Produces("application/json")
	public List<ProductModel> list() {
		try {
			String query = "select p from ProductModel p";
			List<ProductModel> products = objEM.createQuery(query, ProductModel.class).getResultList();
			objEM.close();
			
			return products;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@GET
	@Path("/list/{product_id}")
	@Produces("application/json")
	public ProductModel getById(@PathParam("product_id") Long product_id){
		try {
			ProductModel product = objEM.find(ProductModel.class, product_id);
			objEM.close();
			
			return product;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@GET
	@Path("/list/children/{parent_id}")
	@Produces("application/json")
	public List<ProductModel> getChildren(@PathParam("parent_id") Long parent_id){
		try {
			String query = "select p from ProductModel p where parent_id = " + parent_id;
			List<ProductModel> children = objEM.createQuery(query, ProductModel.class).getResultList();
			objEM.close();
			
			return children;
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/insert")
	@Consumes("application/json")
	public Response insert(ProductModel objProduct){	
		try {
			objEM.persist(objProduct);
			objEM.close();
			return Response.status(200).entity("product inserted.").build();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(ProductModel objProduct){
		try {
			objEM.getTransaction().begin();
			objEM.merge(objProduct);
			objEM.getTransaction().commit();
			objEM.close();
			return Response.status(200).entity("product updated.").build();
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@DELETE
	@Path("/remove/{product_id}")
	public Response delete(@PathParam("product_id") Long product_id){
		try {
			ProductModel objProduct = objEM.find(ProductModel.class, product_id);
			objEM.getTransaction().begin();
			objEM.remove(objProduct);
			objEM.getTransaction().commit();
			objEM.close();
			return Response.status(200).entity("product deleted.").build();
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
}
