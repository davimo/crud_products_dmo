package org.dmo.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT")
public class ProductModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prod_id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@ManyToOne
	@JoinColumn(name="parent_id", nullable=true)
	private ProductModel parent;

	@OneToMany(mappedBy="parent")
	private Set<ProductModel> children = new HashSet<ProductModel>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public ProductModel getParent() {
		return parent;
	}

	public void setParent(ProductModel parent) {
		this.parent = parent;
	}

	public Set<ProductModel> getChildren() {
		return children;
	}

	public void setChildren(Set<ProductModel> children) {
		this.children = children;
	}

	public Long getId() {
		return id;
	}
}
