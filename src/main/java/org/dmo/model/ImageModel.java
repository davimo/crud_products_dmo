package org.dmo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="IMAGE")
public class ImageModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="image_id")
	private Long id;
	
	@Column(name="type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private ProductModel product;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public Long getId() {
		return id;
	}
}
